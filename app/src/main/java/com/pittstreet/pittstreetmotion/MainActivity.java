/**
 * Name: Pitt Street
 * Date created: 7/05/16
 * Version: 1.0
 */

package com.pittstreet.pittstreetmotion;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.ToggleButton;

import com.pittstreet.pittstreetmotion.core.MainProcess;
import com.pittstreet.pittstreetmotion.core.Receiver;

public class MainActivity extends AppCompatActivity {

    private Switch switchStepDetection;
    private ToggleButton toggleBtnWifiConnected;
    private ToggleButton toggleBtnGroupOwner;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    private boolean detectMotion;

    private WifiP2pManager.Channel channel;
    private WifiP2pManager manager;
    private Receiver receiver;
    private IntentFilter intentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "DataCollecting");

        //Wifi Initialising:
        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
        receiver = new Receiver(manager, channel, this);

        intentFilter = new IntentFilter();
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        toggleBtnWifiConnected = (ToggleButton) findViewById(R.id.toggleBtnWifiConnected);
        toggleBtnGroupOwner = (ToggleButton) findViewById(R.id.toggleBtnGroupOwner);

        //Step Detection Set Up:
        final MainActivity thisActivity = this;
        switchStepDetection = (Switch) findViewById(R.id.switchStepDetection);
        switchStepDetection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    wakeLock.acquire();
                    detectMotion = true;
                    Log.d("SWITCH", "on");
                    receiver.discoverPeers();
                    new MainProcess(thisActivity, receiver).execute();
                } else {
                    detectMotion = false;
                    wakeLock.release();
                    Log.d("SWITCH", "off");
                }
            }
        });
    }

    public boolean isDetectMotion() {
        return detectMotion;
    }
    public void setToggleBtnWifiConnected(boolean b) {
        toggleBtnWifiConnected.setChecked(b);
    }
    public void setToggleBtnGroupOwner(boolean b) { toggleBtnGroupOwner.setChecked(b); }

    /* register the broadcast receiver with the intent values to be matched */
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, intentFilter);
    }
    /* unregister the broadcast receiver */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

}
