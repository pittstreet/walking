/**
 * Name: Pitt Street
 * Date created: 7/05/16
 * Version: 1.0
 */

package com.pittstreet.pittstreetmotion.core;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.pittstreet.pittstreetmotion.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

//This class is responsible for managing all background tasks
public class MainProcess extends AsyncTask<Void, Integer, Void> {

    private MainActivity mainActivity;
    private StepDetectionSensor step;
    private Receiver receiver;

    public MainProcess(MainActivity mainActivity, Receiver receiver) {
        this.mainActivity = mainActivity;
        this.receiver = receiver;
    }

    protected Void doInBackground(Void...params) {
        Looper.prepare();
        step = new StepDetectionSensor(mainActivity);
        step.startListening();
        while(mainActivity.isDetectMotion()) {
            if(receiver.isConnected()) {
                detectFootSteps();
            }
        }
        return null;
    }

    protected void onProgressUpdate(Integer... progress) {
        if(progress[0] == 35) {
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
        }
    }

    protected void onPostExecute(Void result) {
        step.stopListening();
    }

    //Transmits to main phone when a foot step is detected
    private void detectFootSteps() {
        boolean isReceiver = receiver.getCurrentInfo().isGroupOwner;

        if(!isReceiver) {
            if(step.isStepDetected()) {
                Socket socket = new Socket();
                try {
                    //(Address, port), timeout (ms)
                    Log.d("SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Creating Socket");
                    socket.bind(null);
                    socket.connect((new InetSocketAddress(receiver.getCurrentInfo().groupOwnerAddress, 1234)), 200);

                    OutputStream outputStream = socket.getOutputStream();
                    Log.d("SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Writing to Socket");
                    outputStream.write(35);

                    outputStream.close();

                } catch (IOException e) {
                    Log.e("SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), e.getMessage());
                } catch (Exception e) {
                    Log.e("SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Something went wrong :(");
                } finally {
                    if(socket != null) {
                        if(socket.isConnected()) {
                            try {
                                socket.close();
                            } catch (IOException e) {
                                Log.e("SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Could not close");
                            } catch (Exception e) {
                                Log.e("SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Disconnect went wrong somehow!");
                            }
                        }
                    }
                }
            }
        } else {
            try {
                ServerSocket serverSocket = new ServerSocket(1234);
                Socket client = serverSocket.accept();
                Log.d("RECEIVE", "Service Successful");

                InputStream inputStream = client.getInputStream();

                int input = inputStream.read();
                Log.d("RECEIVE", "Result: " + input);

                inputStream.close();
                serverSocket.close();

                if(input == 35) publishProgress(35);

            } catch (IOException e) {
                Log.e("SERVER SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Creation Failed");
            } catch (Exception e) {
                Log.e("SERVER SOCKET: " + receiver.getCurrentInfo().groupOwnerAddress.getHostAddress(), "Something went wrong :(");
            }
        }
    }

}
