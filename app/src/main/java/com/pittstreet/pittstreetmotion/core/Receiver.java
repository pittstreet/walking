/**
 * Name: Pitt Street
 * Date created: 27/05/16
 * Version: 1.0
 */

package com.pittstreet.pittstreetmotion.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import com.pittstreet.pittstreetmotion.MainActivity;

public class Receiver extends BroadcastReceiver{

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private MainActivity activity;
    private WifiP2pInfo currentInfo;

    private boolean isConnected;

    public Receiver(WifiP2pManager manager, WifiP2pManager.Channel channel, MainActivity activity) {
        super();
        this.manager = manager;
        this.channel = channel;
        this.activity = activity;

        isConnected = false;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity

            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {

            } else {

            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            if(manager != null) {
                manager.requestPeers(channel, new WifiP2pManager.PeerListListener() {
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        for(WifiP2pDevice device : peers.getDeviceList()) {
                            if(!isConnected) {
                                Log.d("CONNECT", "Connecting to Peer: " + device.deviceName + ", " + device.deviceAddress);
                                connectToPeer(device);
                            }
                        }
                    }
                });
            }
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // Respond to new connection or disconnections
            NetworkInfo networkState = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
            WifiP2pInfo wifiInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);
            currentInfo = wifiInfo;
            if(wifiInfo.isGroupOwner) {
                Log.d("CONNECT", "Group Owner");
                activity.setToggleBtnGroupOwner(false);
            } else {
                activity.setToggleBtnGroupOwner(true);
            }
            WifiP2pDevice device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);

            if(networkState.isConnected()) {
                Log.d("CONNECT", wifiInfo.groupOwnerAddress.getHostAddress());
                isConnected = true;
                if(device == null) Log.e("CONNECT", "Null Device?");
                Log.d("CONNECT", "Connection Established");
                activity.setToggleBtnWifiConnected(true);
            } else {
                isConnected = false;
                Log.d("DISCONNECT", "Disconnected");
                activity.setToggleBtnWifiConnected(false);
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
        }
    }

    public void discoverPeers() {
        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {
                Log.e("CONNECT", "Failed to start discovering peers: " + reason);
            }
        });
    }

    private void connectToPeer(final WifiP2pDevice peer) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = peer.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        config.groupOwnerIntent = 0; //Change this to 15 for receiving device
        manager.connect(channel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {
                Log.e("CONNECT", "Connection Attempt Failed: " + peer.deviceName + " Error: " + reason);
            }
        });
    }

    public boolean isConnected() {
        return isConnected;
    }

    public WifiP2pInfo getCurrentInfo() {
        return currentInfo;
    }

}
